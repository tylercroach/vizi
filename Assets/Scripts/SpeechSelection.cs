﻿using System;
using System.Text;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using UnityEngine.UI;
using System.Collections;

public class SpeechSelection : MonoBehaviour
	{
	public string[] keyword1;
	public string[] keyword2;
	public string[] keyword3;
	public string[] keyword4;
	public string[] keyword5;
	public string[] keyword6;
	public string[] keyword7;

	LoadMainScene startvizi = new LoadMainScene();

	InfoScript info = new InfoScript();

	public GameObject player, models, spaces, tracks, infoobject, finishedbutton, menubuttons;

	private KeywordRecognizer m_Recognizer1;
	private KeywordRecognizer m_Recognizer2;
	private KeywordRecognizer m_Recognizer3;
	private KeywordRecognizer m_Recognizer4;
	private KeywordRecognizer m_Recognizer5;
	private KeywordRecognizer m_Recognizer6;
	private KeywordRecognizer m_Recognizer7;


	void Start()
		{
		startvizi = menubuttons.GetComponent<LoadMainScene>();
		info = infoobject.GetComponent<InfoScript>();
		m_Recognizer1 = new KeywordRecognizer(keyword1);
		m_Recognizer1.OnPhraseRecognized += OnKeyword1Recognized;
		m_Recognizer1.Start();
		m_Recognizer2 = new KeywordRecognizer(keyword2);
		m_Recognizer2.OnPhraseRecognized += OnKeyword2Recognized;
		m_Recognizer2.Start();
		m_Recognizer3 = new KeywordRecognizer(keyword3);
		m_Recognizer3.OnPhraseRecognized += OnKeyword3Recognized;
		m_Recognizer3.Start();
		m_Recognizer4 = new KeywordRecognizer(keyword4);
		m_Recognizer4.OnPhraseRecognized += OnKeyword4Recognized;
		m_Recognizer4.Start();
		m_Recognizer5 = new KeywordRecognizer(keyword5);
		m_Recognizer5.OnPhraseRecognized += OnKeyword5Recognized;
		m_Recognizer5.Start();
		m_Recognizer6 = new KeywordRecognizer(keyword6);
		m_Recognizer6.OnPhraseRecognized += OnKeyword6Recognized;
		m_Recognizer6.Start();
		m_Recognizer7 = new KeywordRecognizer(keyword7);
		m_Recognizer7.OnPhraseRecognized += OnKeyword7Recognized;
		m_Recognizer7.Start();
		}

	private void OnKeyword1Recognized(PhraseRecognizedEventArgs args)
		{
		if (models.activeSelf == true)
			{
			player.GetComponent<Models>().SetModel();
			}
		else if (spaces.activeSelf == true)
			{
			player.GetComponent<Spaces>().SetSpace();
			}
		else if (tracks.activeSelf == true)
			{
			player.GetComponent<Tracks>().SetTrack();
			}
		}
	private void OnKeyword2Recognized(PhraseRecognizedEventArgs args)
		{
		if (models.activeSelf == true)
			{
			player.GetComponent<Models>().ReturnToMenu();
			}
		else if (spaces.activeSelf == true)
			{
			player.GetComponent<Spaces>().ReturnToMenu();
			}
		else if (tracks.activeSelf == true)
			{
			player.GetComponent<Tracks>().ReturnToMenu();
			}
		}
	private void OnKeyword3Recognized(PhraseRecognizedEventArgs args)
		{
		if (models.activeSelf == true)
			{
			GetComponent<AudioSource>().Play();
			player.GetComponent<Models>().SelectModel();
			}
		else if (spaces.activeSelf == true)
			{
			GetComponent<AudioSource>().Play();
			player.GetComponent<Spaces>().SelectSpace();
			}
		else if (tracks.activeSelf == true)
			{
			GetComponent<AudioSource>().Play();
			player.GetComponent<Tracks>().SelectTrack();
			}
		}
	private void OnKeyword4Recognized(PhraseRecognizedEventArgs args)
		{
		if (models.activeSelf == true)
			{
			info.ShowModel();
			}
		else if (spaces.activeSelf == true)
			{
			info.ShowSpace();
			}
		else if (tracks.activeSelf == true)
			{
			info.ShowTrack();
			}
		}
	private void OnKeyword5Recognized(PhraseRecognizedEventArgs args)
		{
		if (models.activeSelf == true)
			{
			info.HideModel();
			}
		else if (spaces.activeSelf == true)
			{
			info.HideSpace();
			}
		else if (tracks.activeSelf == true)
			{
			info.HideTrack();
			}
		}
	private void OnKeyword6Recognized(PhraseRecognizedEventArgs args)
		{
		if (spaces.activeSelf == true)
			{
			player.GetComponent<Spaces>().Preview();
			}
		else if (tracks.activeSelf == true)
			{
			player.GetComponent<Tracks>().Preview();
			}
		}
	private void OnKeyword7Recognized(PhraseRecognizedEventArgs args)
		{
		if (finishedbutton.activeSelf == true) {
			startvizi.ButtonPressed();
			}
		}
	}

