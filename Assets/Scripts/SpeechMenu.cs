﻿using System;
using System.Text;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using UnityEngine.UI;
using System.Collections;

public class SpeechMenu : MonoBehaviour
	{
	public string[] keyword1;
	public string[] keyword2;
	public string[] keyword3;

	public GameObject player;

	private KeywordRecognizer m_Recognizer1;
	private KeywordRecognizer m_Recognizer2;
	private KeywordRecognizer m_Recognizer3;


	void Start()
		{
		m_Recognizer1 = new KeywordRecognizer(keyword1);
		m_Recognizer1.OnPhraseRecognized += OnKeyword1Recognized;
		m_Recognizer1.Start();
		m_Recognizer2 = new KeywordRecognizer(keyword2);
		m_Recognizer2.OnPhraseRecognized += OnKeyword2Recognized;
		m_Recognizer2.Start();
		m_Recognizer3 = new KeywordRecognizer(keyword3);
		m_Recognizer3.OnPhraseRecognized += OnKeyword3Recognized;
		m_Recognizer3.Start();
		}

	private void OnKeyword1Recognized(PhraseRecognizedEventArgs args)
		{
		player.GetComponent<Models>().LoadModels();
		}
	private void OnKeyword2Recognized(PhraseRecognizedEventArgs args)
		{
		player.GetComponent<Spaces>().LoadSpaces();
		}
	private void OnKeyword3Recognized(PhraseRecognizedEventArgs args)
		{
		player.GetComponent<Tracks>().LoadTracks();
		}
	}
