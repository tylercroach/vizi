﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Intro : MonoBehaviour
	{
	public GameObject logo;
	public AudioSource sound1, sound2, warp, fireworks, water, ambience;
	int fade = 4;
	bool isFading = false;
	Fader fader = new Fader();
	// Use this for initialization
	void Start()
		{
		fader = transform.GetChild(0).GetComponent<Fader>();
		StartCoroutine(Animate());
		}

	// Update is called once per frame
	void Update()
		{
		if (isFading)
			{
			fireworks.volume = fireworks.volume - (Time.deltaTime / (fade + 1));
			water.volume = water.volume - (Time.deltaTime / (fade + 1));
			ambience.volume = ambience.volume - (Time.deltaTime / (fade + 1));
			}
		}
	IEnumerator Animate()
		{
		yield return new WaitForSeconds(5);
		iTween.MoveTo(logo, new Vector3(0, 3, 8), 4);
		iTween.ScaleBy(logo, new Vector3(10, 10, 10), 4);
		sound1.Play();
		yield return new WaitForSeconds(5);
		sound2.Play();
		yield return new WaitForSeconds(2);
		iTween.PunchScale(logo, new Vector3(0.75f, 0.75f, 0.75f), 5f);
		warp.Play();
		yield return new WaitForSeconds(3);
		fader.StartFadeOut();
		isFading = true;
		yield return new WaitForSeconds(5.8f);
		SceneManager.LoadScene("Splash");
		}
	}
