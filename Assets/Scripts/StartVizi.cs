﻿using UnityEngine;
using System.Collections;

public class StartVizi : MonoBehaviour
	{

	// Use this for initialization
	void Start()
		{
		StartCoroutine(ActivateChildren());
		}

	// Update is called once per frame
	void Update()
		{

		}
	IEnumerator ActivateChildren()
		{
		yield return new WaitForSeconds(1);
		foreach (Transform child in transform)
			{
			child.gameObject.SetActive(true);
			}
		}
	}
