﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class MainMenu : MonoBehaviour {
	public GameObject buttons, music;
	public AudioSource hover, loading;
	GameObject loader, handlight;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		loader = GameObject.Find("Loader");
		handlight = GameObject.Find("Handlight");
	}
	public void ActivateMenu()
		{
		StartCoroutine(Enable());
		}
	public void DeactivateMenu()
		{
		buttons.transform.localScale = new Vector3(0.15f, 0.15f, 0.15f);
		buttons.SetActive(false);
		}
	public void Return()
		{
		StartCoroutine(LoadMenu());
		}
	IEnumerator Enable()
		{
		buttons.SetActive(true);
		transform.GetChild(0).gameObject.GetComponent<AudioSource>().Play();
		iTween.ScaleBy(buttons, new Vector3(5, 5, 5), 1f);
		yield return new WaitForSeconds(2);
		}
	IEnumerator LoadMenu()
		{
		hover.Play();
		music.GetComponent<AudioSource>().Stop();
		Camera.main.GetComponent<Fader>().StartFadeOut();
		yield return new WaitForSeconds(1f);
		loading.Play();
		Destroy(loader);
		Destroy(handlight);
		yield return new WaitForSeconds(1f);
		SceneManager.LoadScene("Menu");	
		}
}
