﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Spaces : MonoBehaviour
	{
	Transform[] spacetransform;
	public AudioSource whoosh, select;
	public GameObject menubuttons, spacesui, logo, eva, sphere, soundeffects, stars, ambience, voicepanel, evapreview;
	private GameObject spaceobject;
	GameObject panosphere;
	public bool isPano = false;
	public bool isEnviro = false;
	public GameObject[] environments;
	public Texture[] panos;
	Vector3 spacepos = new Vector3(0.02f, 0.9f, 0.5f);
	Quaternion spacerot;
	public Material panomat;
	public bool isSelected = false;
	bool preview = false;
	Fader fader = new Fader();
	int i;
	public string space;
	public Text spacetitle, spacename, scale, height, width, type, selected;
	int vertexcount, materialcount;
	public Transform spaceparent;
	// Use this for initialization
	void Start()
		{
		fader = Camera.main.GetComponent<Fader>();
		i = 0;
		menubuttons.SetActive(true);
		voicepanel.SetActive(true);
		logo.GetComponent<AudioSource>().Play();
		iTween.ScaleAdd(logo, new Vector3(0.075f, 0, 0), 3.5f);
		StartCoroutine(MenuButtons());
		spacesui.SetActive(false);
		spacetitle.text = "";
		}

	// Update is called once per frame
	void Update()
		{
		foreach (Transform child in spaceparent)
			{
			if (!preview)
				{
				child.Rotate(0, 0.5f, 0);
				}
			}
		}

	public void LoadSpaces()
		{
		StartCoroutine(Load());
		}
	//Load spaces into scene
	IEnumerator Load()
		{
		if (eva != null)
			{
			eva.GetComponent<AudioSource>().Stop();
			}
		yield return new WaitForSeconds(0.25f);
		menubuttons.transform.GetChild(1).GetChild(3).GetComponent<AudioSource>().Play();
		fader.StartFadeOut();
		soundeffects.GetComponent<AudioSource>().Play();
		if (eva != null)
			{
			eva.transform.GetChild(1).gameObject.SetActive(true);
			}
		iTween.ScaleAdd(logo, new Vector3(-0.075f, 0, 0), 2f);
		yield return new WaitForSeconds(2.5f);
		menubuttons.SetActive(false);
		voicepanel.SetActive(false);
		logo.GetComponent<AudioSource>().Play();
		iTween.ScaleAdd(logo, new Vector3(0.075f, 0, 0), 3.5f);
		spacesui.SetActive(true);

		if (spaceparent.childCount == 0)
			{
			foreach (Texture pano in panos)
				{
				panosphere = Instantiate(sphere, spacepos, spacerot) as GameObject;
				panomat = new Material(Shader.Find(" Diffuse"));
				panomat.name = pano.name;
				panomat.mainTexture = pano;
				panosphere.name = pano.name;
				panosphere.layer = 8;
				panosphere.GetComponent<Renderer>().material = panomat;
				panosphere.tag = "Panorama";
				panosphere.transform.parent = spaceparent;
				}

			foreach (GameObject space in environments)
				{
				spaceobject = Instantiate(space, spacepos, spacerot) as GameObject;
				spaceobject.name = spaceobject.name.Replace("(Clone)", "");
				spaceobject.transform.parent = spaceparent;
				spaceobject.tag = "Environment";
				spaceobject.layer = 8;
				spacetransform = spaceobject.GetComponentsInChildren<Transform>();
				foreach (Transform transform in spacetransform)
					{
					transform.gameObject.layer = 8;
					}
				}
			}
		i = 0;
		SetSpace();
		}
	//Activates and deactivates space based on child count of space parent
	public void SetSpace()
		{
		spaceparent.GetComponent<AudioSource>().Play();
		foreach (Transform child in spaceparent)
			{
			child.gameObject.SetActive(false);
			}
		if (i < spaceparent.childCount)
			{
			spaceparent.GetChild(i).gameObject.SetActive(true);

			spacetitle.text = spaceparent.GetChild(i).gameObject.name;
			spacename.text = "Name: " + spaceparent.GetChild(i).gameObject.name;
			type.text = "Type: " + spaceparent.GetChild(i).tag;
			if (spaceparent.GetChild(i).tag == "Panorama")
				{
				height.text = "Height: " + spaceparent.GetChild(i).GetComponent<MeshRenderer>().material.mainTexture.height.ToString();
				width.text = "Width: " + spaceparent.GetChild(i).GetComponent<MeshRenderer>().material.mainTexture.width.ToString();
				}
			else if (spaceparent.GetChild(i).tag == "Environment")
				{
				height.text = "Scale: " + spaceparent.GetChild(i).lossyScale.ToString("F4");
				width.text = "";
				}
			i++;
			}
		else if (i == spaceparent.childCount)
			{
			i = 0;
			spaceparent.GetChild(i).gameObject.SetActive(true);
			spacetitle.text = spaceparent.GetChild(i).gameObject.name;
			spacename.text = "Name: " + spaceparent.GetChild(i).gameObject.name;
			type.text = "Type: " + spaceparent.GetChild(i).tag;
			height.text = "Scale: " + spaceparent.GetChild(i).lossyScale.ToString("F4");
			width.text = "";
			i++;
			}
		}
	//Selects space to display and sets name to string for later use
	public void SelectSpace()
		{
		select.Play();
		foreach (Transform child in spaceparent)
			{
			if (child.gameObject.activeSelf == true && child.gameObject.tag == "Environment")
				{
				space = child.gameObject.name;
				isEnviro = true;
				isPano = false;
				selected.text = "Currently Selected Space: " + space;
				isSelected = true;
				spacesui.transform.GetChild(2).GetComponent<AudioSource>().Play();
				ReturnToMenu();
				}
			else if (child.gameObject.activeSelf == true && child.gameObject.tag == "Panorama")
				{
				space = child.gameObject.name;
				isEnviro = false;
				isPano = true;
				selected.text = "Currently Selected Space: " + space;
				isSelected = true;
				spacesui.transform.GetChild(2).GetComponent<AudioSource>().Play();
				ReturnToMenu();
				}
			}
		}

	public void ReturnToMenu()
		{
		if (eva != null)
			{
			eva.transform.GetChild(1).gameObject.GetComponent<AudioSource>().Stop();
			}
		StartCoroutine(Menu());
		}
	//Destroys spaces, scales panels down and returns to main menu
	IEnumerator Menu()
		{
		soundeffects.GetComponent<AudioSource>().Play();
		iTween.ScaleAdd(logo, new Vector3(-0.075f, 0, 0), 2f);
		fader.StartFadeOut();
		yield return new WaitForSeconds(2.5f);
		foreach (Transform child in spaceparent)
			{
			Destroy(child.gameObject);
			}
		logo.SetActive(true);
		Start();
		}
	IEnumerator MenuButtons()
		{
		foreach (Transform child in menubuttons.transform)
			{
			child.localScale = new Vector3(0.0056f, 0.0056f, 0.0056f);
			}
		yield return new WaitForSeconds(1f);
		iTween.ScaleBy(menubuttons.transform.GetChild(0).gameObject, new Vector3(2, 2, 2), 0.5f);
		menubuttons.transform.GetChild(0).GetChild(0).gameObject.GetComponent<AudioSource>().Play();
		yield return new WaitForSeconds(0.5f);
		iTween.ScaleBy(menubuttons.transform.GetChild(1).gameObject, new Vector3(2, 2, 2), 0.5f);
		menubuttons.transform.GetChild(1).GetChild(0).gameObject.GetComponent<AudioSource>().Play();
		yield return new WaitForSeconds(0.5f);
		iTween.ScaleBy(menubuttons.transform.GetChild(2).gameObject, new Vector3(2, 2, 2), 0.5f);
		menubuttons.transform.GetChild(2).GetChild(0).gameObject.GetComponent<AudioSource>().Play();
		yield return new WaitForSeconds(0.5f);
		iTween.ScaleBy(menubuttons.transform.GetChild(3).gameObject, new Vector3(2, 2, 2), 0.5f);
		menubuttons.transform.GetChild(3).GetChild(0).gameObject.GetComponent<AudioSource>().Play();
		}
	public void Preview()
		{
		StartCoroutine(PreviewSpace());
		}
	IEnumerator PreviewSpace()
		{
		foreach (Transform child in spaceparent)
			{
			if (child.gameObject.activeSelf == true)
				{
				if (eva != null)
					{
					eva.transform.GetChild(1).GetComponent<AudioSource>().Stop();
					}
				evapreview.SetActive(true);
				yield return new WaitForSeconds(1.5f);
				whoosh.Play();
				yield return new WaitForSeconds(0.5f);
				preview = true;
				evapreview.SetActive(false);
				iTween.ScaleBy(child.gameObject, new Vector3(100, 100, 100), 3f);
				iTween.MoveTo(child.gameObject, new Vector3(0, 0, -0.5f), 3f);
				child.transform.LookAt(Camera.main.transform);
				spacesui.SetActive(false);
				logo.SetActive(false);
				stars.SetActive(false);
				ambience.SetActive(false);
				spacetitle.gameObject.SetActive(false);
				yield return new WaitForSeconds(5);
				child.localPosition = new Vector3(0.02f, 0.1000061f, 0.292f);
				iTween.ScaleBy(child.gameObject, new Vector3(0.01f, 0.01f, 0.01f), 1f);
				logo.SetActive(true);
				spacetitle.gameObject.SetActive(true);
				spacesui.SetActive(true);
				whoosh.Play();
				stars.SetActive(true);
				ambience.SetActive(true);
				preview = false;
				}
			}
		}
	}
