﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Splash : MonoBehaviour {
	public GameObject hand, text, player;
    public Material full, trans;
	public AudioSource select, move;
	AnimateSplash animator = new AnimateSplash();
   
	// Use this for initialization
	void Start () {
		animator = player.GetComponent<AnimateSplash>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	//Detect if player touches hand
    void OnTriggerEnter(Collider other)
        {
        if (other.gameObject.tag == "Hand")
            {
			animator.Animate();
            }
        }
    }