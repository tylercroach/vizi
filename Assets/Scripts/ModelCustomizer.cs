﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ModelCustomizer : MonoBehaviour
	{
	public List<Color> colors = new List<Color>();
	public List<string>animations = new List<string>();
	GameObject model;
	int a, c;
	// Use this for initialization
	void Start()
		{
		a = 0;
		c = 0;
		}

	// Update is called once per frame
	void Update()
		{
		model = GameObject.FindGameObjectWithTag("Model");

		}
	public void ChangeColor()
		{
		foreach (Transform child in model.transform)
			{
			if (child.childCount > 0)
				{
				foreach (Transform child1 in child)
					{
					if (child1.GetComponent<MeshRenderer>() != null)
						{
						child1.GetComponent<MeshRenderer>().material.color = colors[c];
						}
					if (child1.GetComponent<SkinnedMeshRenderer>() != null)
						{
						child1.GetComponent<SkinnedMeshRenderer>().material.color = colors[c];
						}
					}
				}

			if (child.GetComponent<MeshRenderer>() != null)
				{
				child.GetComponent<MeshRenderer>().material.color = colors[c];
				}
			if (child.GetComponent<SkinnedMeshRenderer>() != null)
				{
				child.GetComponent<SkinnedMeshRenderer>().material.color = colors[c];
				}
			}
		if (model.GetComponent<MeshRenderer>() != null) {
			model.GetComponent<MeshRenderer>().material.color = colors[c];
		}
		if (model.GetComponent<SkinnedMeshRenderer>() != null) {
			model.GetComponent<SkinnedMeshRenderer>().material.color = colors[c];
		}
		c++;
			if (c == colors.Count)
				{
				c = 0;
				}
			}
	public void ChangeAnimation()
		{
		if (animations.Count == 0)
			{
			foreach (AnimationState state in model.GetComponent<Animation>())
				{
				animations.Add(state.name);
				}
			}
			model.GetComponent<Animation>().Play(animations[a]);
			a++;
			if (a == animations.Count)
				{
				a = 0;
				}
			}
		}