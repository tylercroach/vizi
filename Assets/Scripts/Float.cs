﻿using UnityEngine;
using System.Collections;

public class Float : MonoBehaviour
    {
    public float amplitude, frequency;
    private float initialY = 0, timeElapsed = 0;

    void Start() {
        initialY = transform.position.y;
    }
	
	void Update() {
        timeElapsed += Time.deltaTime;
        Vector3 pos = transform.position;
        transform.position = new Vector3(pos.x, initialY + Mathf.Sin(timeElapsed * Mathf.PI * 2 * frequency) * amplitude, pos.z);
	}
}
