﻿using System;
using System.Text;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using UnityEngine.UI;
using System.Collections;

public class SpeechSplash : MonoBehaviour
	{
	public string[] keyword1;

	public GameObject player;

	private KeywordRecognizer m_Recognizer1;


	void Start()
		{
		m_Recognizer1 = new KeywordRecognizer(keyword1);
		m_Recognizer1.OnPhraseRecognized += OnKeyword1Recognized;
		m_Recognizer1.Start();
		}

	private void OnKeyword1Recognized(PhraseRecognizedEventArgs args)
		{
		player.GetComponent<AnimateSplash>().Animate();
		}
	}
