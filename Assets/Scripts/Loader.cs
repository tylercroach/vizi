﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;
using Leap.Unity;
using UnityEngine.SceneManagement;

public class Loader : MonoBehaviour {
	public GameObject sphere, player;
	Object modelprefab, enviroprefab;
	GameObject model, space, panoprefab, music, modelparent, eva, loader, menubuttons;
	AudioClip track;
	Texture pano;
	public static bool sceneLoaded = false;
	Material panomat;
	Transform[] spacetransform;
	string modelname, spacename, trackname;
	bool isPano;
	bool isEnviro;
	// Use this for initialization
	void Start () {
		}
	
	// Update is called once per frame
	void Update () {
		menubuttons = GameObject.Find("Menu Buttons");
		eva = GameObject.Find("Eva");
			music = GameObject.Find("Music");
			modelparent = GameObject.Find("ModelParent");
		if (SceneManager.GetActiveScene().name == "Menu")
			{
			panomat = player.GetComponent<Spaces>().panomat;
			modelname = player.GetComponent<Models>().model;
			spacename = player.GetComponent<Spaces>().space;
			trackname = player.GetComponent<Tracks>().track;
			isPano = player.GetComponent<Spaces>().isPano;
			isEnviro = player.GetComponent<Spaces>().isEnviro;
			}

		if (SceneManager.GetActiveScene().name == "Main" && !sceneLoaded)
			{	
			sceneLoaded = true;
			Load();
			}
		if (menubuttons.GetComponent<LoadMainScene>().buttonPressed == true)
			{
			sceneLoaded = false;
			}
	}
	void Load()
		{
		modelprefab = Resources.Load("Models/" + modelname) as Object;
		model = Instantiate(modelprefab, new Vector3(0, 0.9f, 0.5f), Quaternion.Euler(0, 0, 0)) as GameObject;
		model.tag = "Model";
		model.transform.SetParent(modelparent.transform);

		if (isPano == true)
			{
			panoprefab = Instantiate(sphere, new Vector3(0, 0.9f, 0), Quaternion.Euler(0, 0, 0)) as GameObject;
			panoprefab.transform.localScale = new Vector3(5000, 5000, 5000);
			pano = Resources.Load("Spaces/" + spacename) as Texture;
			panomat.mainTexture = pano;
			panoprefab.GetComponent<MeshRenderer>().material = panomat;
			panoprefab.tag = "Space";
			panoprefab.layer = 8;
			}
		else if (isEnviro == true)
			{
			enviroprefab = Resources.Load("Spaces/" + spacename) as Object;
			space = Instantiate(enviroprefab, new Vector3(0, 0, 0), Quaternion.Euler(0, 180, 0)) as GameObject;
			space.tag = "Space";
			spacetransform = space.GetComponentsInChildren<Transform>();
				foreach (Transform transform in spacetransform)
					{
					transform.gameObject.layer = 8;
					}
			iTween.ScaleBy(space, new Vector3(100, 100, 100), 0.1f);
			iTween.MoveTo(space, new Vector3(0, 0, -0.5f), 0.1f);
			}
		track = Resources.Load("Tracks/" + trackname) as AudioClip;
		music.GetComponent<AudioSource>().clip = track;
		music.GetComponent<AudioSource>().Play();

		}
}
