﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Models : MonoBehaviour
	{
	public GameObject menubuttons, modelsui, logo, eva, soundeffects, voicepanel;
	public GameObject[] modelprefabs;
	static bool evaDestroyed = false;
	GameObject modelobject;
	Vector3 modelpos = new Vector3(0.02f, 0.9f, 0.5f);
	Quaternion modelrot;
	int i;
	public AudioSource select;
	public bool isSelected = false;
	Fader fader = new Fader();
	public string model;
	public Text modeltitle, modelname, vertices, materials, animations, scale, type, selected;
	int vertexcount, materialcount;
	public Transform modelparent;
	// Use this for initialization
	void Start()
		{
		fader = Camera.main.GetComponent<Fader>();
		i = 0;
		menubuttons.SetActive(true);
		voicepanel.SetActive(true);
		logo.GetComponent<AudioSource>().Play();
		iTween.ScaleAdd(logo, new Vector3(0.075f, 0, 0), 3.5f);
		StartCoroutine(MenuButtons());
		modelsui.SetActive(false);
		modeltitle.text = "";
		}

	// Update is called once per frame
	void Update()
		{
		foreach (Transform child in modelparent)
			{
			child.Rotate(0, 0.5f, 0);
			}
		}

	public void LoadModels()
		{
		StartCoroutine(Load());
		}
	//Load models into scene
	IEnumerator Load()
		{
		if (eva != null)
			{
			eva.GetComponent<AudioSource>().Stop();
			}
		yield return new WaitForSeconds(0.25f);
		menubuttons.transform.GetChild(0).GetChild(3).GetComponent<AudioSource>().Play();
		fader.StartFadeOut();
		soundeffects.GetComponent<AudioSource>().Play();
		if (eva != null)
			{
			eva.transform.GetChild(0).gameObject.SetActive(true);
			}
		iTween.ScaleAdd(logo, new Vector3(-0.075f, 0, 0), 2f);
		yield return new WaitForSeconds(2.5f);
		menubuttons.SetActive(false);
		voicepanel.SetActive(false);
		logo.GetComponent<AudioSource>().Play();
		iTween.ScaleAdd(logo, new Vector3(0.075f, 0, 0), 3.5f);
		modelsui.SetActive(true);
		if (modelparent.childCount == 0)
			{
			foreach (GameObject model in modelprefabs)
				{
				modelobject = Instantiate(model, modelpos, modelrot) as GameObject;
				modelobject.name = modelobject.name.Replace("(Clone)", "");
				modelobject.transform.parent = modelparent;
				modelobject.tag = "Model";
				}
			}
		SetModel();
		}
	//Activates and deactivates model based on child count of model parent
	public void SetModel()
		{
		modelparent.GetComponent<AudioSource>().Play();
		materialcount = 0;
		vertexcount = 0;
		foreach (Transform child in modelparent)
			{
			child.gameObject.SetActive(false);
			}
		if (i < modelparent.childCount)
			{
			modelparent.GetChild(i).gameObject.SetActive(true);
			modeltitle.text = modelparent.GetChild(i).gameObject.name;
			modelname.text = "Name: " + modelparent.GetChild(i).gameObject.name;
			scale.text = "Scale: " + modelparent.GetChild(i).localScale.ToString("F3");
			type.text = "Type: 3D " + modelparent.GetChild(i).tag;
			if (modelparent.GetChild(i).GetComponent<MeshRenderer>() != null)
				{
				materials.text = "Materials: " + modelparent.GetChild(i).GetComponent<MeshRenderer>().materials.Length;
				}
			else if (modelparent.GetChild(i).GetComponent<SkinnedMeshRenderer>() != null)
				{
				materials.text = "Materials: " + modelparent.GetChild(i).GetComponent<SkinnedMeshRenderer>().materials.Length;
				}
			else if (modelparent.GetChild(i).GetComponentInChildren<MeshRenderer>() != null)
				{
				foreach (Transform child in modelparent.GetChild(i))
					{
					if (child.GetComponent<MeshRenderer>() != null)
						{
						materialcount = child.GetComponent<MeshRenderer>().materials.Length + materialcount;
						materials.text = "Materials: " + materialcount;
						}
					}
				}
			else if (modelparent.GetChild(i).GetComponentInChildren<SkinnedMeshRenderer>() != null)
				{
				foreach (Transform child in modelparent.GetChild(i))
					{
					if (child.GetComponent<SkinnedMeshRenderer>() != null)
						{
						materialcount = child.GetComponent<SkinnedMeshRenderer>().materials.Length + materialcount;
						materials.text = "Materials: " + materialcount;
						}
					}
				}
			if (modelparent.GetChild(i).GetComponent<MeshFilter>() != null)
				{
				vertices.text = "Vertex Count: " + modelparent.GetChild(i).GetComponent<MeshFilter>().mesh.vertexCount;
				}
			else if (modelparent.GetChild(i).GetComponentInChildren<MeshFilter>() != null)
				{
				foreach (Transform child in modelparent.GetChild(i))
					{
					if (child.GetComponent<MeshFilter>() != null)
						{
						vertexcount = child.GetComponent<MeshFilter>().mesh.vertexCount + vertexcount;
						vertices.text = "Vertex Count: " + vertexcount;
						}
					}
				}
			else
				{
				vertices.text = "Unable to Calculate Vertices";
				}
			if (modelparent.GetChild(i).GetComponentInChildren<Animation>() == null)
				{
				animations.text = "Animations: No";
				}
			else if (modelparent.GetChild(i).GetComponentInChildren<Animation>() != null)
				{
				animations.text = "Animations: Yes";
				}
			i++;
			}
		else if (i == modelparent.childCount)
			{
			i = 0;
			modelparent.GetChild(i).gameObject.SetActive(true);
			modeltitle.text = modelparent.GetChild(i).gameObject.name;
			modelname.text = "Name: " + modelparent.GetChild(i).gameObject.name;
			if (i < modelparent.childCount)
				{
				modelparent.GetChild(i).gameObject.SetActive(true);
				modeltitle.text = modelparent.GetChild(i).gameObject.name;

				if (modelparent.GetChild(i).GetComponent<MeshRenderer>() != null)
					{
					materials.text = "Materials: " + modelparent.GetChild(i).GetComponent<MeshRenderer>().materials.Length;
					}
				else if (modelparent.GetChild(i).GetComponent<SkinnedMeshRenderer>() != null)
					{
					materials.text = "Materials: " + modelparent.GetChild(i).GetComponent<SkinnedMeshRenderer>().materials.Length;
					}
				else if (modelparent.GetChild(i).GetComponentInChildren<MeshRenderer>() != null)
					{
					foreach (Transform child in modelparent.GetChild(i))
						{
						if (child.GetComponent<MeshRenderer>() != null)
							{
							materialcount = child.GetComponent<MeshRenderer>().materials.Length + materialcount;
							materials.text = "Materials: " + materialcount;
							}
						}
					}
				else if (modelparent.GetChild(i).GetComponentInChildren<SkinnedMeshRenderer>() != null)
					{
					foreach (Transform child in modelparent.GetChild(i))
						{
						if (child.GetComponent<SkinnedMeshRenderer>() != null)
							{
							materialcount = child.GetComponent<SkinnedMeshRenderer>().materials.Length + materialcount;
							materials.text = "Materials: " + materialcount;
							}
						}
					}
				if (modelparent.GetChild(i).GetComponent<MeshFilter>() != null)
					{
					vertices.text = "Vertex Count: " + modelparent.GetChild(i).GetComponent<MeshFilter>().mesh.vertexCount;
					}
				else if (modelparent.GetChild(i).GetComponentInChildren<MeshFilter>() != null)
					{
					foreach (Transform child in modelparent.GetChild(i))
						{
						if (child.GetComponent<MeshFilter>() != null)
							{
							vertexcount = child.GetComponent<MeshFilter>().mesh.vertexCount + vertexcount;
							vertices.text = "Vertex Count: " + vertexcount;
							}
						}
					}
				else
					{
					vertices.text = "Unable to Calculate Vertices";
					}
				if (modelparent.GetChild(i).GetComponentInChildren<Animation>() == null)
					{
					animations.text = "Animations: No";
					}
				else if (modelparent.GetChild(i).GetComponentInChildren<Animation>() != null)
					{
					animations.text = "Animations: Yes";
					}
				i++;
				}
			}
		}
	//Selects model to display and sets name to string for later use
	public void SelectModel()
		{
		select.Play();
		foreach (Transform child in modelparent)
			{
			if (child.gameObject.activeSelf == true)
				model = child.gameObject.name;
				selected.text = "Currently Selected Model: " + model;
				isSelected = true;
				modelsui.transform.GetChild(2).GetComponent<AudioSource>().Play();
				ReturnToMenu();
			}
		}

	public void ReturnToMenu()
		{
		if (eva != null)
			{
			eva.transform.GetChild(0).gameObject.GetComponent<AudioSource>().Stop();
			}
		StartCoroutine(Menu());
		}
	//Destroys models, scales panels down and returns to main menu
	IEnumerator Menu() {
		soundeffects.GetComponent<AudioSource>().Play();
		iTween.ScaleAdd(logo, new Vector3(-0.075f, 0, 0), 2f);
		fader.StartFadeOut();
		yield return new WaitForSeconds(2);
		foreach (Transform child in modelparent)
			{
			Destroy(child.gameObject);
			}
		yield return new WaitForSeconds(0.5f);
		logo.SetActive(true);
		Start();
		}
	IEnumerator MenuButtons()
		{
		foreach (Transform child in menubuttons.transform)
			{
			child.localScale = new Vector3(0.0056f, 0.0056f, 0.0056f);
			}
		yield return new WaitForSeconds(1f);
		iTween.ScaleBy(menubuttons.transform.GetChild(0).gameObject, new Vector3(2, 2, 2), 0.5f);
		menubuttons.transform.GetChild(0).GetChild(0).gameObject.GetComponent<AudioSource>().Play();
		yield return new WaitForSeconds(0.5f);
		iTween.ScaleBy(menubuttons.transform.GetChild(1).gameObject, new Vector3(2, 2, 2), 0.5f);
		menubuttons.transform.GetChild(1).GetChild(0).gameObject.GetComponent<AudioSource>().Play();
		yield return new WaitForSeconds(0.5f);
		iTween.ScaleBy(menubuttons.transform.GetChild(2).gameObject, new Vector3(2, 2, 2), 0.5f);
		menubuttons.transform.GetChild(2).GetChild(0).gameObject.GetComponent<AudioSource>().Play();
		yield return new WaitForSeconds(0.5f);
		iTween.ScaleBy(menubuttons.transform.GetChild(3).gameObject, new Vector3(2, 2, 2), 0.5f);
		menubuttons.transform.GetChild(3).GetChild(0).gameObject.GetComponent<AudioSource>().Play();
		}
	}
