﻿using UnityEngine;
using System.Collections;

public class ToggleInfo : MonoBehaviour
	{
	public GameObject modelinfo, spaceinfo, trackinfo, infoobject;
	InfoScript info = new InfoScript();
	// Use this for initialization
	void Start()
		{
		modelinfo.SetActive(false);
		spaceinfo.SetActive(false);
		trackinfo.SetActive(false);
		info = infoobject.GetComponent<InfoScript>();
		}

	// Update is called once per frame
	void Update()
		{

		}
	//Checks if user touches model
	void OnTriggerEnter(Collider other)
		{
		if (other.gameObject.tag == "ModelInfo" && modelinfo.activeSelf == true)
			{
			info.HideModel();
			}
		else if (other.gameObject.tag == "ModelInfo" && modelinfo.activeSelf == false)
			{
			info.ShowModel();
			}
		if (other.gameObject.tag == "SpaceInfo" && spaceinfo.activeSelf == true)
			{
			info.HideSpace();
			}
		else if (other.gameObject.tag == "SpaceInfo" && spaceinfo.activeSelf == false)
			{
			info.ShowSpace();
			}
		if (other.gameObject.tag == "TrackInfo" && trackinfo.activeSelf == true)
			{
			info.HideTrack();
			}
		else if (other.gameObject.tag == "TrackInfo" && trackinfo.activeSelf == false)
			{
			info.ShowTrack();
			}
		}
	}
