﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class AnimateSplash : MonoBehaviour
	{
	public GameObject hand, text, eva;
	public Material full, trans;
	public AudioSource select, move;
	// Use this for initialization
	void Start()
		{
		iTween.ScaleTo(hand, new Vector3(0.04f, 0.04f, 0.04f), 2f);
		iTween.ScaleTo(text, new Vector3(0.05f, 0.05f, 0.05f), 2f);
		}

	public void Animate()
		{
		move.Play();
		Camera.main.GetComponent<Fader>().StartFadeOut();
		hand.GetComponent<BoxCollider>().enabled = false;
		hand.GetComponent<Renderer>().material = full;
		iTween.MoveAdd(hand, new Vector3(0.01f, 0.1f, 0.01f), 10);
		iTween.ScaleTo(hand, iTween.Hash("scale", new Vector3(0.2f, 0.2f, 0.2f), "time", 5, "easetype", iTween.EaseType.linear));
		eva.GetComponent<AudioSource>().Stop();
		iTween.FadeTo(text, 0f, 2f);
		iTween.FadeTo(hand, 0f, 4f);
		StartCoroutine(LoadMenu());
		}
	IEnumerator LoadMenu()
		{
		yield return new WaitForSeconds(5.5f);
		SceneManager.LoadScene("Menu");
		}
	}
