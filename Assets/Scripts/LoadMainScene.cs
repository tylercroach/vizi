﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadMainScene : MonoBehaviour
	{
	public GameObject player, logo, finished, loader;
	public AudioSource padtrans, evaload;
	public bool buttonPressed = false;
	// Use this for initialization
	void Start()
		{
		finished.SetActive(false);
		}
	void Update()
		{
		if (player.GetComponent<Models>().isSelected && player.GetComponent<Spaces>().isSelected)
			{
			finished.transform.parent = transform;
			finished.SetActive(true);
			}
		}
	public void ButtonPressed()
		{
		buttonPressed = true;
		player.transform.GetChild(0).GetComponent<Fader>().fadeOutTime = 6;
		player.transform.GetChild(0).GetComponent<Fader>().fadeInTime = 2;
		player.transform.GetChild(0).GetComponent<Fader>().StartFadeOut();
		padtrans.Play();
		iTween.ScaleAdd(logo, new Vector3(-0.075f, 0, 0), 7.5f);
		StartCoroutine(Load());
		}
	IEnumerator Load()
		{
		yield return new WaitForSeconds(3.5f);
		evaload.Play();
		buttonPressed = false;
		yield return new WaitForSeconds(3);
		SceneManager.LoadScene("Main");
		}
	}
