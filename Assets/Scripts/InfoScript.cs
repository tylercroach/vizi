﻿using UnityEngine;
using System.Collections;

public class InfoScript : MonoBehaviour {
	public GameObject modelinfo, spaceinfo, trackinfo;
	// Use this for initialization
	void Start () {
	
	}
	
	public void HideModel()
		{
		StartCoroutine(HideModelInfo());
		}
	public void ShowModel()
		{
		StartCoroutine(ShowModelInfo());
		}
	public void HideSpace()
		{
		StartCoroutine(HideSpaceInfo());
		}
	public void ShowSpace()
		{
		StartCoroutine(ShowSpaceInfo());
		}
	public void HideTrack()
		{
		StartCoroutine(HideTrackInfo());
		}
	public void ShowTrack()
		{
		StartCoroutine(ShowTrackInfo());
		}

	//Hides info panel
	IEnumerator HideModelInfo()
		{
		modelinfo.transform.GetChild(0).GetComponent<AudioSource>().Play();
		iTween.ScaleTo(modelinfo, new Vector3(0, 0, 0), 1.5f);
		yield return new WaitForSeconds(1.5f);
		modelinfo.SetActive(false);
		}
	//Shows info panel
	IEnumerator ShowModelInfo()
		{
		iTween.ScaleTo(modelinfo, new Vector3(0.0005f, 0.00116197f, 0.0005029134f), 1f);
		yield return new WaitForSeconds(0.5f);
		modelinfo.SetActive(true);
		modelinfo.GetComponent<AudioSource>().Play();
		}
	//Shows info panel
	IEnumerator ShowSpaceInfo()
		{
		iTween.ScaleTo(spaceinfo, new Vector3(0.0005f, 0.00116197f, 0.0005029134f), 1f);
		yield return new WaitForSeconds(0.5f);
		spaceinfo.SetActive(true);
		spaceinfo.GetComponent<AudioSource>().Play();
		}
		IEnumerator HideSpaceInfo()
		{
		spaceinfo.transform.GetChild(0).GetComponent<AudioSource>().Play();
		iTween.ScaleTo(spaceinfo, new Vector3(0, 0, 0), 1.5f);
		yield return new WaitForSeconds(1.5f);
		spaceinfo.SetActive(false);
		}
	//Shows info panel
	IEnumerator ShowTrackInfo()
		{
		iTween.ScaleTo(trackinfo, new Vector3(0.0005f, 0.00116197f, 0.0005029134f), 1f);
		yield return new WaitForSeconds(0.5f);
		trackinfo.SetActive(true);
		trackinfo.GetComponent<AudioSource>().Play();
		}
		IEnumerator HideTrackInfo()
		{
		trackinfo.transform.GetChild(0).GetComponent<AudioSource>().Play();
		iTween.ScaleTo(trackinfo, new Vector3(0, 0, 0), 1.5f);
		yield return new WaitForSeconds(1.5f);
		trackinfo.SetActive(false);
		}
}
