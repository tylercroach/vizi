﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Tracks : MonoBehaviour
	{
	public GameObject menubuttons, tracksui, logo, eva, trackobject, soundeffects, voicepanel, evapreview;
	public AudioSource select;
	GameObject panosphere;
	public AudioClip[] tracks;
	Vector3 trackpos = new Vector3(0.02f, 0.9f, 0.5f);
	Fader fader = new Fader();
	Quaternion trackrot;
	int i;
	public bool isSelected = false;
	public string track;
	public Text tracktitle, trackname, duration, type, frequency, selected;
	int vertexcount, materialcount;
	public Transform trackparent;
	int fade = 6;
	bool preview = false;
	// Use this for initialization
	void Start()
		{
		logo.GetComponent<AudioSource>().Play();
		fader = Camera.main.GetComponent<Fader>();
		i = 0;
		menubuttons.SetActive(true);
		voicepanel.SetActive(true);
		logo.GetComponent<AudioSource>().Play();
		iTween.ScaleAdd(logo, new Vector3(0.075f, 0, 0), 3.5f);
		StartCoroutine(MenuButtons());
		tracksui.SetActive(false);
		tracktitle.text = "";
		}

	// Update is called once per frame
	void Update()
		{
		foreach (Transform child in trackparent)
			{
			if (child.gameObject.activeSelf == true && preview)
				{
				if (child.GetComponent<AudioSource>().volume < 0.5)
					{
					child.GetComponent<AudioSource>().volume = child.GetComponent<AudioSource>().volume + (Time.deltaTime / (fade + 1));
					}
				}
			else if (child.gameObject.activeSelf == true && !preview)
				{
				if (child.GetComponent<AudioSource>().volume > 0)
					{
					Debug.Log("FadeOut");
					child.GetComponent<AudioSource>().volume = child.GetComponent<AudioSource>().volume - (Time.deltaTime / (fade + 1));
					}
				}
			else if (child.GetComponent<AudioSource>().volume == 0)
					{
					child.GetComponent<AudioSource>().Stop();
					}
				}
			}

	public void LoadTracks()
		{
		StartCoroutine(Load());
		}
	//Load tracks into scene
	IEnumerator Load()
		{
		if (eva != null)
			{
			eva.GetComponent<AudioSource>().Stop();
			}
		yield return new WaitForSeconds(0.25f);
		menubuttons.transform.GetChild(2).GetChild(3).GetComponent<AudioSource>().Play();
		fader.StartFadeOut();
		soundeffects.GetComponent<AudioSource>().Play();
		if (eva != null)
			{
			eva.transform.GetChild(2).gameObject.SetActive(true);
			}
		iTween.ScaleAdd(logo, new Vector3(-0.075f, 0, 0), 2f);
		yield return new WaitForSeconds(2.5f);
		menubuttons.SetActive(false);
		voicepanel.SetActive(false);
		logo.GetComponent<AudioSource>().Play();
		iTween.ScaleAdd(logo, new Vector3(0.075f, 0, 0), 3.5f);
		tracksui.SetActive(true);
		if (trackparent.childCount == 0)
			{
			foreach (AudioClip track in tracks)
				{
				trackobject = Instantiate(trackobject, trackpos, trackrot) as GameObject;
				trackobject.GetComponent<AudioSource>().clip = track;
				trackobject.name = track.name.Replace("(Clone)", "");
				trackobject.transform.parent = trackparent;
				trackobject.tag = "Track";
				}
			}
		i = 0;
		SetTrack();
		}
	//Activates and deactivates track based on child count of track parent
	public void SetTrack()
		{
		preview = true;
		trackparent.GetComponent<AudioSource>().Play();
		foreach (Transform child in trackparent)
			{
			child.GetComponent<AudioSource>().Stop();
			child.gameObject.SetActive(false);
			}
		if (i < trackparent.childCount)
			{
			trackparent.GetChild(i).gameObject.SetActive(true);
			tracktitle.text = trackparent.GetChild(i).gameObject.name;
			trackname.text = "Name: " + trackparent.GetChild(i).gameObject.name;
			type.text = "Type: " + trackparent.GetChild(i).tag;
			duration.text = "Duration: " + trackparent.GetChild(i).GetComponent<AudioSource>().clip.length.ToString("F0") + " seconds";
			frequency.text = "Frequency: " + trackparent.GetChild(i).GetComponent<AudioSource>().clip.frequency + " hz";
			i++;
			}
		else if (i == trackparent.childCount)
			{
			i = 0;
			trackparent.GetChild(i).gameObject.SetActive(true);
			tracktitle.text = trackparent.GetChild(i).gameObject.name;
			trackname.text = "Name: " + trackparent.GetChild(i).gameObject.name;
			type.text = "Type: " + trackparent.GetChild(i).tag;
			duration.text = "Duration: " + trackparent.GetChild(i).GetComponent<AudioSource>().clip.length/60;
			frequency.text = "Frequency: " + trackparent.GetChild(i).GetComponent<AudioSource>().clip.frequency + " hz";
			i++;
			}
		}
	//Selects track to display and sets name to string for later use
	public void SelectTrack()
		{
		foreach (Transform child in trackparent)
			{
			if (child.gameObject.activeSelf == true)
				{
				track = child.gameObject.name;
				selected.text = "Currently Selected Track: " + track;
				isSelected = true;
				select.Play();
				tracksui.transform.GetChild(2).GetComponent<AudioSource>().Play();
				ReturnToMenu();
				}
			}
		}

	public void ReturnToMenu()
		{
		if (eva != null)
			{
			eva.transform.GetChild(2).gameObject.GetComponent<AudioSource>().Stop();
			}
		StartCoroutine(Menu());
		}
	//Destroys tracks, scales panels down and returns to main menu
	IEnumerator Menu()
		{
		soundeffects.GetComponent<AudioSource>().Play();
		iTween.ScaleAdd(logo, new Vector3(-0.075f, 0, 0), 2f);
		fader.StartFadeOut();
			foreach (Transform child in trackparent)
			{
			child.gameObject.GetComponent<AudioSource>().Stop();
			}
		yield return new WaitForSeconds(2.5f);
		foreach (Transform child in trackparent)
			{
			child.gameObject.SetActive(false);
			}
		logo.SetActive(true);
		Start();
		}
	IEnumerator MenuButtons()
		{
		foreach (Transform child in menubuttons.transform)
			{
			child.localScale = new Vector3(0.0056f, 0.0056f, 0.0056f);
			}
		yield return new WaitForSeconds(1f);
		iTween.ScaleBy(menubuttons.transform.GetChild(0).gameObject, new Vector3(2, 2, 2), 0.5f);
		menubuttons.transform.GetChild(0).GetChild(0).gameObject.GetComponent<AudioSource>().Play();
		yield return new WaitForSeconds(0.5f);
		iTween.ScaleBy(menubuttons.transform.GetChild(1).gameObject, new Vector3(2, 2, 2), 0.5f);
		menubuttons.transform.GetChild(1).GetChild(0).gameObject.GetComponent<AudioSource>().Play();
		yield return new WaitForSeconds(0.5f);
		iTween.ScaleBy(menubuttons.transform.GetChild(2).gameObject, new Vector3(2, 2, 2), 0.5f);
		menubuttons.transform.GetChild(2).GetChild(0).gameObject.GetComponent<AudioSource>().Play();
		yield return new WaitForSeconds(0.5f);
		iTween.ScaleBy(menubuttons.transform.GetChild(3).gameObject, new Vector3(2, 2, 2), 0.5f);
		menubuttons.transform.GetChild(3).GetChild(0).gameObject.GetComponent<AudioSource>().Play();
		}
	public void Preview()
		{
		StartCoroutine(Previewtrack());
		}
	IEnumerator Previewtrack()
		{
		preview = false;
		if (eva != null)
			{
			eva.transform.GetChild(2).GetComponent<AudioSource>().Stop();
			}
		evapreview.SetActive(true);
		yield return new WaitForSeconds(1.5f);
		foreach (Transform child in trackparent)
			{
			if (child.gameObject.activeSelf == true && !preview)
				{
				preview = true;
				evapreview.SetActive(false);
				child.GetComponent<AudioSource>().Play();
				yield return new WaitForSeconds(10);
				preview = false;
				}
			}
		}
	}
